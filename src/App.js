// import logo from './logo.svg';
import './App.css';
import { useState, useEffect } from 'react';
import Table from './Table';
import Newplayer from './Newplayer';
import Searchplayer from './Searchplayer';

function App() {
  // const [books, setBooks] = useState([]);
  // useEffect(
  //   () => {
  //     setBooks([
  //       {
  //         nama: "Buku A",
  //         harga: "Rp 12.000"
  //       },
  //       {
  //         nama: "Buku B",
  //         harga: "Rp 14.000"
  //       },
  //       {
  //         nama: "Buku C",
  //         harga: "Rp 16.000"
  //       }
  //     ])
  //   },
  //   []
  // )

  // const onChange = () => {
  //   setBooks([
  //     {
  //       nama: "Buku D",
  //       harga: "Rp 11.000"
  //     },
  //     {
  //       nama: "Buku E",
  //       harga: "Rp 13.000"
  //     },
  //     {
  //       nama: "Buku F",
  //       harga: "Rp 15.000"
  //     }
  //   ])
  // }

  // const masukVariable = () => {
  //   onChange = {(e) => setName(e.target.value)
  //   }
  // }


  return (
    <div className="App">
      {/* <p className="pag">Table Buku</p>
      <label>Nama</label>
      <br></br>
      <input onChange={masukVariable(this)}></input>
      <br></br>
      <label>Harga</label>
      <br></br>
      <input></input>
      <Table books={books} onChange={onChange} /> */}
      <Newplayer />
      <Searchplayer />
    </div>
  );
}

export default App;
